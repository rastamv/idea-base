# LIVE DEMO

![](https://s6.gifyu.com/images/livedemo.gif)

Dodatkowe uwagi:
  - Cały theme postawiłem na 'blanku' mając na uwadze, że 'luma' jest raczej do wglądu jak tworzyć theme niż aby na jego podstawie robić własny theme.
  - Na dostarczonym screenie nie było informacje w jaki sposób powinien wyświetlać się search więc zostawiłem tak jak jest to natywnie (dlatego jest tam trochę więcej miejsca).
  - Brak dostarczonych customowych fontów i ikonek więc dodałem je z innych źródeł (dlatego różnią się od tych co na dostarczonym screenie).
 
